import pkgutil
from io import StringIO

import pytest

from cron.exceptions import OutOfBoundsError
from cron.main import main
from cron.core import CronTask, Cron

_test_data = [
    ("16:10", "30 1", "1:30", False),
    ("1:30", "30 1", "1:30", True),
    ("1:10", "30 1", "1:30", True),
    ("00:10", "30 1", "1:30", True),
    ("16:10", "45 *", "16:45", True),
    ("16:45", "45 *", "16:45", True),
    ("20:15", "45 *", "20:45", True),
    ("16:00", "45 *", "16:45", True),
    ("16:10", "* *", "16:10", True),
    ("00:00", "* *", "0:00", True),
    ("16:10", "* 19", "19:00", True),
    ("19:00", "* 19", "19:00", True),
    ("19:10", "* 19", "19:10", True),
    ("20:10", "* 19", "19:00", False),
    ("19:59", "* 19", "19:59", True),
    ("20:00", "* 19", "19:00", False),
]


@pytest.fixture
def input_test_text():
    return pkgutil.get_data("data", "test_input").decode()


@pytest.fixture
def output_test_text():
    return pkgutil.get_data("data", "test_output").decode()


def _get_cron_task(now: str, input_: str):
    cron = Cron.parse_string_time(now)
    task = CronTask.parse_string(f"{input_} /foo")

    return cron, task


@pytest.mark.parametrize(
    "value, exception",
    [("-5", OutOfBoundsError), ("60", OutOfBoundsError), ("a", ValueError), ("", ValueError)],
)
def test_parse_time_raises(value, exception):
    with pytest.raises(exception):
        assert CronTask._parse_time_int(value, 60)


@pytest.mark.parametrize("value", ["0", "30", "59"])
def test_parse_time(value):
    assert CronTask._parse_time_int(value, 60) == int(value)


@pytest.mark.parametrize(
    "now, input_, output", [(*x[:2], x[2]) for x in _test_data],
)
def test_next_event_string(now, input_, output):
    cron, task = _get_cron_task(now, input_)

    assert Cron.time_to_str(cron.get_next_occurrence(task)) == output


@pytest.mark.parametrize(
    "now, input_, output", [(*x[:2], x[3]) for x in _test_data],
)
def test_is_today(now, input_, output):
    cron, task = _get_cron_task(now, input_)

    assert cron.occurs_today(cron.get_next_occurrence(task)) == output


def test_file(capsys, monkeypatch, input_test_text, output_test_text):
    monkeypatch.setattr("sys.argv", ["foo", "16:10"])
    monkeypatch.setattr("sys.stdin", StringIO(input_test_text))

    main()

    assert capsys.readouterr().out == output_test_text
