import argparse
import logging
import sys
from argparse import Namespace

from cron.core import Cron, CronTask
from cron.exceptions import OutOfBoundsError, MalformedConfigLineError, NoInputDataError

TODAY_STR = "today"
TOMORROW_STR = "tomorrow"

logger = logging.getLogger()


def get_parser(args) -> Namespace:
    parser = argparse.ArgumentParser(
        description="Compute next occurrences for a set of cron job definitions"
    )
    parser.add_argument("time_now", type=str, default=None)
    return parser.parse_args(args)


def main():
    if not sys.stdin.isatty():
        args = get_parser(sys.argv[1:])
        tasks = CronTask.parse_list_jobs(sys.stdin.readlines())
        cron = Cron.parse_string_time(args.time_now)

        for task in tasks:
            next_occurrence = cron.get_next_occurrence(task)
            print(
                "{} {} - {}".format(
                    Cron.time_to_str(next_occurrence),
                    TODAY_STR if cron.occurs_today(next_occurrence) else TOMORROW_STR,
                    task.command,
                )
            )
    else:
        raise NoInputDataError("Missing input in stdin")


if __name__ == "__main__":
    try:
        main()
    except (ValueError, OutOfBoundsError, MalformedConfigLineError, NoInputDataError) as e:
        logger.error("Error with your input: %s", e)
        exit(1)
