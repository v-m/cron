class OutOfBoundsError(Exception):
    pass


class MalformedConfigLineError(Exception):
    pass


class NoInputDataError(Exception):
    pass
