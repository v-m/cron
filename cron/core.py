from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import Optional, List

from cron.exceptions import OutOfBoundsError, MalformedConfigLineError


@dataclass
class CronTask:
    """A cron task definition."""

    minute: Optional[int]
    hour: Optional[int]
    command: str

    @staticmethod
    def _parse_time_int(value: str, max_value: int) -> int:
        """
        Convert a string value into its integer value if it is in range [0;max_value]
        :param value: the string value to convert
        :param max_value: maximum range
        """
        try:
            if max_value > (parsed := int(value)) >= 0:
                return parsed
            raise OutOfBoundsError(f"{value} don't belong to range [0;{max_value}]")
        except ValueError:
            if value == "*":
                return None
            raise ValueError(f"'{value}' is not a legit value")

    def __post_init__(self):
        self.minute = self._parse_time_int(self.minute, 60)
        self.hour = self._parse_time_int(self.hour, 24)

    @staticmethod
    def parse_list_jobs(list_: List[str]) -> List["CronTask"]:
        """
        Parse a list of string, each entry containing a cron task description
        """
        return [CronTask.parse_string(x) for x in list_]

    @staticmethod
    def parse_string_list_jobs(string: str) -> List["CronTask"]:
        """
        Parse a string where each line containing a cron task description
        """
        return CronTask.parse_list_jobs(string.split("\n"))

    @staticmethod
    def parse_string(string: str) -> "CronTask":
        """
        Read a cron task description from it's string value
        """
        try:
            p = string.strip().split()
            return CronTask(*p)
        except TypeError:
            raise MalformedConfigLineError("Wrong number of line items")

    @property
    def has_hour(self) -> bool:
        return self.hour is None

    @property
    def has_minute(self) -> bool:
        return self.minute is None


class Cron:
    def __init__(self, now_time: Optional[datetime]):
        self._now = now_time or datetime.now()

    @staticmethod
    def parse_string_time(time_str: str):
        """
        Create a Cron object with the current time set to time_str
        """
        try:
            hour, minute = map(int, time_str.split(":"))
        except ValueError:
            raise ValueError("Wrong current time format")

        now_ = datetime.now()
        return Cron(now_.replace(hour=hour, minute=minute))

    def get_next_occurrence(self, task: CronTask) -> datetime:
        """
        Compute the datetime for the next occurrence of a task based on this Cron object current time
        """
        if not task.has_hour and task.has_minute and self._now.hour != task.hour:
            target_time = self._now.replace(hour=task.hour, minute=0)
        else:
            target_time = self._now.replace(
                hour=task.hour or self._now.hour, minute=task.minute or self._now.minute,
            )

        if task.has_hour and not task.has_minute and self._now > target_time:
            target_time += timedelta(hours=1)

        if self._now > target_time:
            target_time += timedelta(days=1)

        return target_time

    def occurs_today(self, target_time: datetime) -> bool:
        """
        Determines if a datetime occurs "today" according to this Cron internal current time
        """
        return self._now.day == target_time.day

    @staticmethod
    def time_to_str(target_time: datetime) -> str:
        """
        Format a datetime to a string.

        Note:
            Using target_time.time().strftime("%H:%M") would be better but it will produce a zero padded hour
            which may not comply with the expected output
        """
        return "{}:{:0>2}".format(target_time.hour, target_time.minute)
