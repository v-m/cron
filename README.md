# Cron

Compute next occurrences for a set of cron job definitions (hours and minutes).
These job definitions are passed through `stdin` using the following format:

```
30 1 /bin/run_me_daily
45 * /bin/run_me_hourly
* * /bin/run_me_every_minute
* 19 /bin/run_me_sixty_times
```

And will produce an output such as:

```
1:30 tomorrow - /bin/run_me_daily
16:45 today - /bin/run_me_hourly
16:10 today - /bin/run_me_every_minute
19:00 today - /bin/run_me_sixty_times
```

## Running

This project requires Python 3.8. 
It has no dependencies and can easily be run using `poetry`.
To run the project:

```
$ poetry install
$ poetry run cron 16:10 < data/test_input
```

If `python` command points to an valid Python 3.8 installation,
one of the following can be used as well:

```
$ ./cron.sh 16:10 < data/test_input
$ python -m cron.main 16:10 < data/test_input
```

## Running tests

Some basic tests have been added to the project. 
They however require `pytest` to be run.
To run these, using poetry:

```
$ poetry run pytest
```

Again, using python only:

```
# Use a virtual environment is recommended
$ pip install pytest
$ pytest
```

Note that these tests are far from being exhaustive but just ensure a minimal coverage.